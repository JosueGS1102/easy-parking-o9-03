package com.example.easyparkingO0903;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyparkingO0903Application {

	public static void main(String[] args) {
		SpringApplication.run(EasyparkingO0903Application.class, args);
	}

}
