/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.easyparkingO0903.dao;

import com.example.easyparkingO0903.model.Gerente;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author josue
 */
public interface GerenteDao extends CrudRepository<Gerente, Integer>{
    
}
