/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.easyparkingO0903.dao;

import com.example.easyparkingO0903.model.Operador;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author josue
 */
public interface OperadorDao extends CrudRepository<Operador, Integer>{
    
}
