
package com.example.easyparkingO0903.service.implement;
import com.example.easyparkingO0903.dao.GerenteDao;
import com.example.easyparkingO0903.model.Gerente;
import com.example.easyparkingO0903.service.GerenteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author josue
 */
@Service
public class GerenteServiceImpl implements GerenteService{
    @Autowired
    private GerenteDao gerentedao;
    
    @Override
    @Transactional(readOnly=false)
    public Gerente save (Gerente gerente){
        return gerentedao.save(gerente);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id){
        gerentedao.deleteById(id);
    }
    @Override
    @Transactional(readOnly=true)
    public Gerente findById(Integer id){
        return gerentedao.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly=true)
    public List<Gerente> findAll(){
        return (List<Gerente>) gerentedao.findAll();
    }
}
