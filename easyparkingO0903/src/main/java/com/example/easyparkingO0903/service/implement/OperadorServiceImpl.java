
package com.example.easyparkingO0903.service.implement;
import com.example.easyparkingO0903.dao.OperadorDao;
import com.example.easyparkingO0903.model.Operador;
import com.example.easyparkingO0903.service.OperadorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author josue
 */
@Service
public class OperadorServiceImpl implements OperadorService{
    @Autowired
    private OperadorDao operadordao;
    
    @Override
    @Transactional(readOnly=false)
    public Operador save (Operador operador){
        return operadordao.save(operador);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id){
       operadordao.deleteById(id);
    }
    @Override
    @Transactional(readOnly=true)
    public Operador findById(Integer id){
        return operadordao.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly=true)
    public List<Operador> findAll(){
        return (List<Operador>) operadordao.findAll();
    }
}
