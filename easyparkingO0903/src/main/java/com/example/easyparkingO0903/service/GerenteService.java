/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.easyparkingO0903.service;

import com.example.easyparkingO0903.model.Gerente;
import java.util.List;

/**
 *
 * @author josue
 */
public interface GerenteService {
    public Gerente save (Gerente gerente);
    public void delete (Integer id);
    public Gerente findById(Integer id);
    public List<Gerente> findAll();
}
