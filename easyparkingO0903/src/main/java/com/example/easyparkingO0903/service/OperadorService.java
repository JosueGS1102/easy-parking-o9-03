/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.easyparkingO0903.service;

import com.example.easyparkingO0903.model.Operador;
import java.util.List;

/**
 *
 * @author josue
 */
public interface OperadorService {
    public Operador save (Operador operador);
    public void delete (Integer id);
    public Operador findById(Integer id);
    public List<Operador> findAll();
}
