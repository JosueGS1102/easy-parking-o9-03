
package com.example.easyparkingO0903.service.implement;
import com.example.easyparkingO0903.dao.ClienteDao;
import com.example.easyparkingO0903.model.Cliente;
import com.example.easyparkingO0903.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author josue
 */
@Service
public class ClienteServiceImpl implements ClienteService{
    @Autowired
    private ClienteDao clientedao;
    
    @Override
    @Transactional(readOnly=false)
    public Cliente save (Cliente cliente){
        return clientedao.save(cliente);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete (Integer id){
        clientedao.deleteById(id);
    }
    @Override
    @Transactional(readOnly=true)
    public Cliente findById(Integer id){
        return clientedao.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly=true)
    public List<Cliente> findAll(){
        return (List<Cliente>) clientedao.findAll();
    }
}
