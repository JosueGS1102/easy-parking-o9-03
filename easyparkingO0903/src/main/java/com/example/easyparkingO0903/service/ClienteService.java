/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.easyparkingO0903.service;

import com.example.easyparkingO0903.model.Cliente;
import java.util.List;

/**
 *
 * @author josue
 */
public interface ClienteService {
    public Cliente save (Cliente cliente);
    public void delete (Integer id);
    public Cliente findById(Integer id);
    public List<Cliente> findAll();
    
}
