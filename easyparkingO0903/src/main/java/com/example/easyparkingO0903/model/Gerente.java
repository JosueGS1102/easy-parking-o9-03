
package com.example.easyparkingO0903.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Gerente")
public class Gerente implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="idClave")
    private Integer idclave;
    
    @Column(name="Nombre")
    private String nombre;
    
    @Column(name="Fecha")
    private int fecha;
    
    @Column(name="Cedula")
    private int cedula;
    
    @Column(name="Cargo")
    private String cargo;

    public Integer getIdclave() {
        return idclave;
    }

    public void setIdclave(Integer idclave) {
        this.idclave = idclave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFecha() {
        return fecha;
    }

    public void setFecha(int fecha) {
        this.fecha = fecha;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    
}
