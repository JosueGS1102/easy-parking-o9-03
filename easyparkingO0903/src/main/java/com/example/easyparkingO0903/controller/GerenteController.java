
package com.example.easyparkingO0903.controller;
import com.example.easyparkingO0903.model.Gerente;
import com.example.easyparkingO0903.service.GerenteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author josue
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/Gerente")
public class GerenteController {
    @Autowired
    private GerenteService gerenteservice;

    @PostMapping(value = "/")
    public ResponseEntity<Gerente> agregar(@RequestBody Gerente gerente) {
        Gerente obj = gerenteservice.save(gerente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Gerente> eliminar(@PathVariable Integer id) {
        Gerente obj = gerenteservice.findById(id);
        if (obj != null) {
            gerenteservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Gerente> editar(@RequestBody Gerente gerente) {
        Gerente obj = gerenteservice.findById(gerente.getIdclave());
        if (obj != null) {
            obj.setNombre(gerente.getNombre());
            obj.setFecha(gerente.getFecha());
            obj.setCedula(gerente.getCedula());
            obj.setCargo(gerente.getCargo());
            gerenteservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Gerente> consultarTodo() {
        return gerenteservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Gerente consultaPorId(@PathVariable Integer id) {
        return gerenteservice.findById(id);
    }
}
