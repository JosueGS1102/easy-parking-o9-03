
package com.example.easyparkingO0903.controller;
import com.example.easyparkingO0903.model.Operador;
import com.example.easyparkingO0903.service.OperadorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author josue
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/Operador")
public class OperadorController {
    @Autowired
    private OperadorService operadorservice;

    @PostMapping(value = "/")
    public ResponseEntity<Operador> agregar(@RequestBody Operador operador) {
        Operador obj = operadorservice.save(operador);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Operador> eliminar(@PathVariable Integer id) {
        Operador obj = operadorservice.findById(id);
        if (obj != null) {
            operadorservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Operador> editar(@RequestBody Operador operador) {
        Operador obj = operadorservice.findById(operador.getIdcodigo());
        if (obj != null) {
            obj.setNombre(operador.getNombre());
            obj.setFecha(operador.getFecha());
            obj.setCedula(operador.getCedula());
            obj.setCargo(operador.getCargo());
            operadorservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Operador> consultarTodo() {
        return operadorservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Operador consultaPorId(@PathVariable Integer id) {
        return operadorservice.findById(id);
    }
}
