
package com.example.easyparkingO0903.controller;
import com.example.easyparkingO0903.model.Cliente;
import com.example.easyparkingO0903.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author josue
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/Cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteservice;

    @PostMapping(value = "/")
    public ResponseEntity<Cliente> agregar(@RequestBody Cliente cliente) {
        Cliente obj = clienteservice.save(cliente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id) {
        Cliente obj = clienteservice.findById(id);
        if (obj != null) {
            clienteservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente) {
        Cliente obj = clienteservice.findById(cliente.getIdCedula());
        if (obj != null) {
            obj.setNombre(cliente.getNombre());
            obj.setTelefono(cliente.getTelefono());
            obj.setNumeroPlaca(cliente.getNumeroPlaca());
            obj.setTipoVehiculo(cliente.getTipoVehiculo());
            clienteservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Cliente> consultarTodo() {
        return clienteservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Cliente consultaPorId(@PathVariable Integer id) {
        return clienteservice.findById(id);
    }
}
